//
//  SocialHelper.swift
//  Calculator
//
//  Created by Dmitriy Bagrov on 09.12.15.
//
//

import Foundation
import UIKit
import Social

class SocialHelper {
    
    // MARK: - Present publication Dialog
    
    func presentPublicationDialog(viewController: UIViewController, text: String, serviceType: String) {
        if SLComposeViewController.isAvailableForServiceType(serviceType) {
            let composeVC = SLComposeViewController(forServiceType: serviceType)
            composeVC.setInitialText(text)
            viewController.presentViewController(composeVC, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""),
                message: NSLocalizedString("You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup", comment: ""),
                preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            viewController.presentViewController(alertController, animated: true, completion: nil)
        }
    }
}
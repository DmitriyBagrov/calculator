//
//  Constants.swift
//  Calculator
//
//  Created by Dmitriy Bagrov on 04.12.15.
//
//

import Foundation
import UIKit

// MARK: - UI Constants

let screenWidth = UIScreen.mainScreen().bounds.size.width

let screenHeight = UIScreen.mainScreen().bounds.size.height

let statusBarHeight: CGFloat = 20.0

// MARK: - Enumerations

// MARK: - Managers

let calcManager = CalcManager()

// MARK: - Formatters

let maxFractionDigits = 10

let kNumberFormatter: NSNumberFormatter = {
    let formatter = NSNumberFormatter()
    formatter.numberStyle = .DecimalStyle
    formatter.maximumFractionDigits = maxFractionDigits
    return formatter
}()

// MARK: - Cell Identifiers

let kCIHistoryOperationTableViewCell = "HistoryOperationTableViewCell"


//
//  HistoryOperationTableViewCell.swift
//  Calculator
//
//  Created by Dmitriy Bagrov on 07.12.15.
//
//

import UIKit

class HistoryOperationTableViewCell: UITableViewCell {

    // MARK: - Properies
    
    var operationSet: OperationModelSet? {
        didSet {
            displayOperationSet()
        }
    }
    
    // MARK: - Display Result
    
    private func displayOperationSet() {
        textLabel?.text = operationSet?.formattedOperations()
    }
}

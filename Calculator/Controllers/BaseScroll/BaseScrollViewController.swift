//
//  BaseScrollViewController.swift
//  Calculator
//
//  Created by Dmitriy Bagrov on 08.12.15.
//
//

import UIKit

class BaseScrollViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - IBOutlets: Constraints
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var middleViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollViewContentWidth: NSLayoutConstraint!
    
    @IBOutlet weak var middleViewTopConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureScrollView()
    }
    
    // MARK: - Configure Methods
    
    private func configureScrollView() {
        scrollViewContentWidth.constant = screenWidth * 2
        middleViewTopConstraint.constant = -statusBarHeight
        topViewHeight.constant = screenHeight - middleViewHeight.constant
    }
    
    // MARK: - Status Bar Appearence
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        if let middle = middleViewTopConstraint where middle.constant > 0 {
            return .Default
        } else {
            return .LightContent
        }
    }
    
    // MARK: - UIGestureRecognizerDelegate
    
    @IBAction func panGesture(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .Began:
            break
        case .Changed:
            panGestureDidChanged(sender)
            break
        case .Ended:
            panGestureDidEnded(sender)
            break
        default:()
        }
    }
    
    // MARK: - Gesture Actions
    
    private func panGestureDidChanged(sender: UIPanGestureRecognizer) {
        let location = sender.locationInView(view)
        middleViewTopConstraint.constant = location.y
    }
    
    private func panGestureDidEnded(sender: UIPanGestureRecognizer) {
        let location = sender.locationInView(view)
        if location.y > screenHeight / 2 {
            middleViewTopConstraint.constant = screenHeight - middleViewHeight.constant
        } else {
            middleViewTopConstraint.constant = -statusBarHeight
        }
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: .CurveEaseIn, animations: { () -> Void in
            self.view.layoutIfNeeded()
            }, completion: nil)
        setNeedsStatusBarAppearanceUpdate()
    }
}

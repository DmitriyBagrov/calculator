//
//  CalculatorViewController.swift
//  Calculator
//
//  Created by Dmitriy Bagrov on 03.12.15.
//
//

import UIKit

class CalculatorViewController: BaseScrollViewController {
    
    // MARK: - IBOutlets: Controls
    
    @IBOutlet weak var inputTextField: UITextField!
    
    // MARK: - Properies
    
    var inputNumber: NSNumber? {
        if let input = inputTextField.text where input.characters.count > 0 {
            return NSNumber(string: input)
        } else {
            return nil
        }
    }
    
    var operands = [NSNumber]()
    
    var operationType: String?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - IBActions
    
    @IBAction func inputButtonTapped(sender: UIButton) {
        if let input = sender.titleLabel?.text {
            inputTextField.text?.appendContentsOf(input)
        } else {
            print("Empty Input")
        }
    }
    
    @IBAction func removeInputButtonTapped(sender: UIButton) {
        if let input = inputTextField.text {
            inputTextField.text = String(input.characters.dropLast())
        }
    }
    
    @IBAction func binaryOperationButtonTapped(sender: UIButton) {
        if let nextOperationType = sender.titleLabel?.text {
            performBinaryOperationIfNeed()
            operationType = nextOperationType
        } else {
            print("Empty Binary Operation")
        }
    }
    
    @IBAction func unaryOperationButtonTapped(sender: UIButton) {
        if let operationType = sender.titleLabel?.text {
            self.operationType = operationType
            performUnaryOperationIfNeed()
        } else {
            print("Empty Unary Operation")
        }
    }
    
    @IBAction func resultButtonTapped(sender: UIButton) {
        //Perform Last Binary Operation
        performBinaryOperationIfNeed()
        operationType = nil
        calcManager.savePerformedOperations()
    }
    
    // MARK: - Private Helpers
    
    private func performBinaryOperationIfNeed() {
        if let input = inputNumber {
            operands.append(input)
        }
        if let operationType = operationType where operands.count == 2 {
            calcManager.performBinaryOperation(operationType, left: operands.first!, right: operands.last!, complete: {
                [unowned self] (result: NSNumber?, error: NSError?) -> Void in
                if let _ = error {
                    //TODO: Handle Error
                } else if let result = result {
                    self.operands = [result]
                }
                })
        }
        displayResult(operands.first)
    }
    
    private func performUnaryOperationIfNeed() {
        if let input = inputNumber {
            operands.append(input)
        }
        if let operationType = operationType, left = operands.last {
            calcManager.performUnaryOperation(operationType, left: left, complete: {
                [unowned self](result: NSNumber?, error: NSError?) -> Void in
                if let _ = error {
                    //TODO: Handle Error
                } else if let result = result {
                    self.operands = [result]
                }
            })
        }
        displayResult(operands.first)
    }
    
    private func displayResult(result: NSNumber?) {
        if let result = result {
            inputTextField.placeholder = kNumberFormatter.stringFromNumber(result)
        } else {
            inputTextField.placeholder = "0"
        }
        inputTextField.text = nil
    }
    
}
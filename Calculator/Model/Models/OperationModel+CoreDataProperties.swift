//
//  OperationModel+CoreDataProperties.swift
//  
//
//  Created by Dmitriy Bagrov on 05.12.15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension OperationModel {

    @NSManaged var uid: NSNumber
    @NSManaged var left: NSNumber
    @NSManaged var right: NSNumber?
    @NSManaged var result: NSNumber
    @NSManaged var operationType: String
    @NSManaged var isUnary: NSNumber
    
    // MARK: - Accessory Methods

    class func create(operationType: String, left: NSNumber, right: NSNumber? = nil, result: NSNumber, isUnary: Bool) -> OperationModel {
        let operationModel = OperationModel.create() as! OperationModel
        operationModel.operationType = operationType
        operationModel.left = left
        operationModel.right = right
        operationModel.result = result
        operationModel.isUnary = isUnary
        return operationModel
    }
    
    // MARK: - Formatter
    
    var unaryFormatt: String {
        return operationType + "(" + left.stringValue + ")"
    }
}

//
//  OperationModelSet+CoreDataProperties.swift
//
//
//  Created by Dmitriy Bagrov on 05.12.15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData
import SwiftRecord

extension OperationModelSet {
    
    @NSManaged var uid: NSNumber
    @NSManaged var operations: NSSet
    
    // MARK: - Accessory Helpers
    
    func addOperation(operation: OperationModel) {
        let operationsCache = self.mutableSetValueForKey("operations")
        operationsCache.addObject(operation)
    }
    
    // MARK: - Formatter
    
    func formattedOperations() -> String {
        let sortedOperations = operations.sort({
            let first = $0 as! OperationModel
            let second = $1 as! OperationModel
            return first.uid < second.uid
        }) as! [OperationModel]
        return sortedOperations.reduce("", combine: { (result, next) -> String in
            var string = ""
            if next == sortedOperations.first {
                if next.isUnary.boolValue {
                    string = result + next.unaryFormatt
                } else {
                    string = result + next.left.stringValue + next.operationType + next.right!.stringValue
                }
            } else {
                if next.isUnary.boolValue {
                    string = result + next.unaryFormatt
                } else {
                    string = result + next.operationType + next.right!.stringValue
                }
            }
            if next == sortedOperations.last {
                string = string + "=" + next.result.stringValue
            }
            return string
        })
    }
}

//
//  OperationModel.swift
//  
//
//  Created by Dmitriy Bagrov on 05.12.15.
//
//

import Foundation
import CoreData
import SwiftRecord

class OperationModel: NSManagedObject {
    
    // MARK: - Properties
    
    override class func autoIncrementingId() -> String? {
        return "uid"
    }

}

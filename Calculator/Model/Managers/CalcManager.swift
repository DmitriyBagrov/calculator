//
//  CalcManager.swift
//  Calculator
//
//  Created by Dmitriy Bagrov on 05.12.15.
//
//

import Foundation
import SwiftRecord

class CalcManager {
    
    // MARK: - Typealias
    
    typealias BinaryOperation = (NSNumber, NSNumber) -> NSNumber
    
    typealias UnaryOpeartion = NSNumber -> NSNumber
    
    typealias Completion = (NSNumber?, NSError?) -> Void
    
    // MARK: - Properies
    
    var binaryOperations: [String : BinaryOperation] = [:]
    //Swift compiler don't like it. =(
//        "+" : { $0 + $1 },
//        "-" : { $0 - $1 },
//        "/" : { $0 / $1 },
//        "*" : { $0 * $1 }
//    ]
    
    var unaryOperations: [String : UnaryOpeartion] = [:]
//        "%"   : { $0.doubleValue / 100 },
//        "1/x" : { 1 / $0.doubleValue },
//        "x2"  : { pow($0.doubleValue, 2) },
//        "x3"  : { pow($0.doubleValue, 3) }
//    ]
    
    var operationModelSet: OperationModelSet?
    
    // MARK: - Lyfecircle
    
    init() {
        configureOperationsStorage()
    }
    
    // MARK: - Configure Methods
    
    private func configureOperationsStorage() {
        //Configure Binary Operations
        binaryOperations["+"]  = { $0 + $1 }
        binaryOperations["-"]  = { $0 - $1 }
        binaryOperations["/"]  = { $0 / $1 }
        binaryOperations["*"]  = { $0 * $1 }
        
        //Configure Unary Operations
        unaryOperations["%"]   = { $0.doubleValue / 100 }
        unaryOperations["1/x"] = { 1 / $0.doubleValue }
        unaryOperations["x2"]  = { pow($0.doubleValue, 2) }
        unaryOperations["x3"]  = { pow($0.doubleValue, 3) }
        unaryOperations["√x"]  = { sqrt($0.doubleValue) }
        unaryOperations["ln"]  = { log10($0.doubleValue) }
        unaryOperations["log"] = { log($0.doubleValue) }
        unaryOperations["cos"] = { cos($0.doubleValue) }
        unaryOperations["sin"] = { sin($0.doubleValue) }
        unaryOperations["tan"] = { tan($0.doubleValue) }
        unaryOperations["ex"]  = { exp($0.doubleValue) }
        unaryOperations["degrees"]  = { $0 * (180 / M_PI) }
    }
    
    // MARK: - Operation perform
    
    func performBinaryOperation(operationType: String, left: NSNumber, right: NSNumber, complete: Completion) {
        if let operation = binaryOperations[operationType] {
            let result = operation(left, right)
            appendOperationModelToSet(OperationModel.create(operationType, left: left, right: right, result: result, isUnary: false))
            complete(result, nil)
        } else {
            //TODO: Unknow Binary Error
        }
    }
    
    func performUnaryOperation(operationType: String, left: NSNumber, complete: Completion) {
        if let operation = unaryOperations[operationType] {
            let result = operation(left)
            appendOperationModelToSet(OperationModel.create(operationType, left: left, result: result, isUnary: true))
            complete(result, nil)
        } else {
            //TODO: Unknow Unary Operation
        }
    }
    
    // MARK: - Operation Managing
    
    func savePerformedOperations(complete: Completion? = nil) {
        operationModelSet?.save()
        operationModelSet = nil
        complete?(nil, nil)
    }
    
    // MARK: - Private Helpers
    
    private func appendOperationModelToSet(operation: OperationModel) {
        if let _ = operationModelSet { } else {
            operationModelSet = OperationModelSet.create() as? OperationModelSet
        }
        operationModelSet?.addOperation(operation)
    }
    
}
//
//  NSNumber+Extension.swift
//  Calculator
//
//  Created by Dmitriy Bagrov on 05.12.15.
//
//

import Foundation


extension NSNumber {
    
    convenience init(string value: String) {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        
        var separator = ""
        
        if let safeSeparator = formatter.decimalSeparator {
            separator = safeSeparator
        }
        
        var safeValue = value
        
        if separator == "," {
            safeValue = value.stringByReplacingOccurrencesOfString(".", withString: ",", options: [], range: nil)
        } else {
            safeValue = value.stringByReplacingOccurrencesOfString(",", withString: ".", options: [], range: nil)
        }
        
        if safeValue.isEmpty {
            safeValue = "0"
        }
        
        let number = formatter.numberFromString(safeValue)
        if let double = number?.doubleValue {
            self.init(double: double)
        } else {
            self.init(double: 0)
        }
    }
    
}

//Infixes

public func + (left: NSNumber, right: NSNumber) -> NSNumber {
    return left.doubleValue + right.doubleValue
}

public func - (left: NSNumber, right: NSNumber) -> NSNumber {
    return left.doubleValue - right.doubleValue
}

public func += (inout left: NSNumber, right: NSNumber) {
    left = left.doubleValue + right.doubleValue
}

public func -= (inout left: NSNumber, right: NSNumber) {
    left = left.doubleValue - right.doubleValue
}

public func * (left: NSNumber, right: NSNumber) -> NSNumber {
    return left.doubleValue * right.doubleValue
}

public func / (left: NSNumber, right: NSNumber) -> NSNumber {
    return left.doubleValue / right.doubleValue
}

public func > (left: NSNumber, right: NSNumber) -> Bool {
    return left.doubleValue > right.doubleValue
}

public func < (left: NSNumber, right: NSNumber) -> Bool {
    return left.doubleValue < right.doubleValue
}

public func <= (left: NSNumber, right: NSNumber) -> Bool {
    return left.doubleValue <= right.doubleValue
}

public func >= (left: NSNumber, right: NSNumber) -> Bool {
    return left.doubleValue >= right.doubleValue
}
